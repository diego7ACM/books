<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/css/user.css">
</head>

<body>

<nav class="navbar navbar-default custom-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button><a class="navbar-brand navbar-link" href="index.php">Books</a></div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav links">
                    <li> <form action="button">
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#exampleModal">Busqueda avanzada</button>
                    <!--inicio del modal-->
                    <div class="modal" tabindex="-1" role="dialog" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Busqueda avanzada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Ingrese la busqueda de su preferencia</p>
        
        <div class="input-group input-group-sm mb-3">
  <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Autor">
</div>  
<br><!--machete-->
<div class="input-group input-group-sm mb-3">
  <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Titulo">
</div>
<br><!--machete-->
  <div class="input-group input-group-sm mb-3">
  <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Indice">
</div>
<br><!--machete-->
      </div>
      <div class="modal-footer">
          <form action="buscar">
        <button type="button" class="btn btn-primary" id="buscar">Buscar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
</form>
      </div>
    </div>
  </div>
</div>
                    </form>
                    <li> <form class="form-inline mr-auto" action="busqueda.php" method="get">
						<div class="search-element mr-3">
							<input class="form-control" type="search" placeholder="Busqueda simple" aria-label="Search" name="busqueda" value="busqueda">
								<button type="submit"  class="btn btn-dark">Buscar</button>
                <span class="Search-icon"><i class="fa fa-search"></i></span>
						</div>
					</form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    </body>

</html>