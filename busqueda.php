<?php 
include ("include/menu.php");
include ("database.php");
$busqueda=$_GET['busqueda'];

$sql="select * from books where titulo='$busqueda' or autor='$busqueda'" ;
//";
$query=mysqli_query($con,$sql);
$row=mysqli_fetch_object($query)
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/css/user.css">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">

<!-- Iconos-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

<body>
<div class="panel-body">   
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Indice </th>
                    <th>Titulo</th>
                    <th>Autor </th>
                    <th>Opciones </th>
                </tr>
            </thead>
            <tbody>
            <?php
                    do{
                ?>
                <tr>
                    <td><?php echo $row->idbooks; ?></td>
                    <td><?php echo $row->titulo; ?></td>
                    <td><?php echo $row->autor; ?></td>
                    <td>
                        <a class="btn btn-info" href="#"><i class="fas fa-edit"></i></a>
                        <button type="submit" class="btn-sm btn-danger"><i class="fas fa-star"></i></button>
                    </td>
                </tr>

                <?php
} while($row=mysqli_fetch_object($query));
                ?>
            </tbody>
        </table>
    </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>