CREATE DATABASE IF NOT EXISTS books;
USE books;
CREATE TABLE `books`.`book`
 ( `id` INT NOT NULL , 
 `title` VARCHAR NOT NULL , 
 `autor` VARCHAR NOT NULL , 
 `calificacion` INT NOT NULL , 
 PRIMARY KEY (`id`)) ENGINE = InnoDB;